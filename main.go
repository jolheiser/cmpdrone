package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/google/go-jsonnet"
	"github.com/sergi/go-diff/diffmatchpatch"
	"gopkg.in/yaml.v3"
)

func main() {
	htmlFlag := flag.Bool("html", false, "Output HTML")
	flag.Parse()

	source, err := os.ReadFile(".drone.jsonnet")
	if err != nil {
		panic(err)
	}
	vm := jsonnet.MakeVM()
	streams, err := vm.EvaluateSnippetStream(".drone.jsonnet", string(source))
	if err != nil {
		panic(err)
	}

	var jsonnetDoc strings.Builder
	for _, stream := range streams {
		var j any
		if err := yaml.Unmarshal([]byte(stream), &j); err != nil {
			panic(err)
		}

		b, err := yaml.Marshal(j)
		if err != nil {
			panic(err)
		}
		jsonnetDoc.WriteString("---\n")
		jsonnetDoc.Write(b)
	}

	curSource, err := os.ReadFile(".drone.yml")
	if err != nil {
		panic(err)
	}

	var currentDoc strings.Builder
	dec := yaml.NewDecoder(bytes.NewReader(curSource))
	for {
		var y any
		err := dec.Decode(&y)
		if err != nil {
			if !errors.Is(err, io.EOF) {
				panic(err)
			}
			break
		}
		b, err := yaml.Marshal(y)
		if err != nil {
			panic(err)
		}
		currentDoc.WriteString("---\n")
		currentDoc.Write(b)
	}

	if jsonnetDoc.String() == currentDoc.String() {
		return
	}

	dmp := diffmatchpatch.New()
	diffs := dmp.DiffMain(currentDoc.String(), jsonnetDoc.String(), false)
	if *htmlFlag {
		fmt.Println(dmp.DiffPrettyHtml(diffs))
		return
	}
	fmt.Println(dmp.DiffPrettyText(diffs))
}
