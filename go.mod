module gitea.com/jolheiser/cmpdrone

go 1.19

require (
	github.com/google/go-jsonnet v0.19.1
	github.com/sergi/go-diff v1.1.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	gopkg.in/yaml.v2 v2.2.7 // indirect
	sigs.k8s.io/yaml v1.1.0 // indirect
)
