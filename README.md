# cmpdrone

Compare a `.drone.jsonnet` end-result with a current `.drone.yaml` to see differences.

Due to how Go serializes, the YAML could change without necessarily being different, so this tool makes a fresh serialization of each for comparison.

## License

[MIT](LICENSE)
